﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using System.Windows.Threading;
using BrowserBL.Models;
using BrowserUI.Helpers;
using BrowserUI.Templates;
using Models.BrowserBL;

namespace BrowserUI
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public const string IMG_DIV_ID_PREFIX = "ImgId_";
        private WebBrowserHelper webHelper;
        private CancellationTokenSource _cts;
        private Task _updateThumbnailsTask;

        public MainWindow()
        {
            InitializeComponent();
            var helper = new ObjectForScriptingHelper(this);
            webBrowser.ObjectForScripting = helper;
        }

        /// <summary>
        ///     Выбрали новый узел в дереве каталогов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DirTV_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (!(e.NewValue is DirectoryModel newDirectory))
            {
                Debug.WriteLine("Exception.");
                return;
            }

            Debug.WriteLine($"Selected {newDirectory.Name}");
            UpdateThumbnails(newDirectory);
        }

        private void Thumbnails_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            webHelper.UpdateBrowser(e.AddedItems.Cast<PreviewImage>(), e.RemovedItems.Cast<PreviewImage>());
        }

        private void UpdateThumbnails(DirectoryModel dirModel)
        {
            if (_cts != null && _cts.Token.CanBeCanceled)
            {
                Debug.WriteLine("Attemp to cancel task=" + _updateThumbnailsTask.Id + " token=" + _cts.GetHashCode());
                _cts.Cancel();
                _cts.Dispose();

            }

            Thumbnails.Items.Clear();
            _cts = new CancellationTokenSource();
            Debug.WriteLine("New task token=" + _cts.GetHashCode());
            _updateThumbnailsTask = Task.Factory.StartNew(() => UpdateThumbnailsAsync(dirModel, _cts.Token), _cts.Token);
            Debug.WriteLine("Start task=" + _updateThumbnailsTask.Id + " token=" + _cts.GetHashCode());

        }

        private void UpdateThumbnailsAsync(DirectoryModel dirModel, CancellationToken cts)
        {
            void DrawCache(IEnumerable<FileInfo> fileInfos)
            {
                Thumbnails.Dispatcher.Invoke(() =>
                    {
                        Debug.WriteLine("...DrawCache " + Thread.CurrentThread.ManagedThreadId + " token=" + cts.GetHashCode());
                        foreach (var cacheItem in fileInfos)
                        {
                            var image = PreviewImage.OpenImage(cacheItem);
                            if (image != null && !cts.IsCancellationRequested)
                            {
                                Thumbnails.Items.Add(image);
                                //Thumbnails.re
                            }
                        }
                    }
                );
            }

            cts.Register(() =>
            {

                Debug.WriteLine("...Delegate Cancelled thread=" + Thread.CurrentThread.ManagedThreadId + " token=" + cts.GetHashCode());
                Thumbnails.Items.Clear();
            });
            Debug.WriteLine("...Started thread " + Thread.CurrentThread.ManagedThreadId + " token=" + cts.GetHashCode());
            var images = dirModel.DirInfo.EnumerateFiles().Where(f => "*.jpg,*.png".Contains(f.Extension.ToLower()));
            Debug.WriteLine("...Images " + images.Count());
            var cache = new List<FileInfo>();
            foreach (var img in images)
            {
                if (cts.IsCancellationRequested)
                {
                    Debug.WriteLine("...Cancel thread request " + Thread.CurrentThread.ManagedThreadId + " token=" + cts.GetHashCode());
                    cts.ThrowIfCancellationRequested();
                }

                cache.Add(img);
                if (cache.Count > 50)
                {
                    DrawCache(cache);
                cache.Clear();
                }

            }
            if (cache.Count > 0)
            {
                DrawCache(cache);
            }
            if (Thumbnails.Items.Count > 0)
            {
                Thumbnails.Dispatcher.Invoke(() => { Thumbnails.ScrollIntoView(Thumbnails.Items[0]); });
            }
            Debug.WriteLine("...Complete thread " + Thread.CurrentThread.ManagedThreadId + " token=" + cts.GetHashCode());
        }

        private void webBrowser_LoadCompleted(object sender, NavigationEventArgs e)
        {
            webHelper = new WebBrowserHelper(webBrowser, ActualWidth);
            var cssToLoad = new List<string>
            {
                TemplateHelper.ImageViewCss.Value,
                $"img {{width: {webHelper.ImageWidth}px; max-width: {webHelper.ImageWidth}px; }}"
            };
#if DEBUG
            cssToLoad.Add(TemplateHelper.ImageViewDebugCss.Value);
#endif
            webHelper.LoadStylesToBrowser(cssToLoad);
            webHelper.LoadScriptsToBrowser(new[] { TemplateHelper.ImageViewTs.Value });
            //TODO Здесь разблокируем дерево или иначе синхронизируемся.
        }

        private void webBrowser_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            webHelper?.UpdateWebBrowserAdaptiveMarkupAlt();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //TODO ImageWidth>=150px
            try
            {
                var imageDir = new DirectoryInfo(Directory.GetCurrentDirectory());
                var imgFolder = imageDir.Parent.Parent.Parent.GetDirectories().Single(d => d.Name == "Images");
                DirTV.ItemsSource = new List<DirectoryModel> { new DirectoryModel(imgFolder) };
                UpdateThumbnails(new DirectoryModel(imgFolder));
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка открытия папки с данными. Свяжитесь с разработчиком.");
                return;
            }

            webBrowser.NavigateToString(TemplateHelper.ImageViewTemplate.Value);
#if DEBUG
            lblDebug.Visibility = Visibility.Visible;
#endif
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //TODO Remove
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            //TODO Remove
        }
    }
}