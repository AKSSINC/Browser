﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BrowserUI.Templates
{
public static class TemplateHelper
    {

        public static Lazy<string> ImageViewTemplate = new Lazy<string>(() => ReadResource("Templates.ImageView.html"));

        public static Lazy<string> ImageViewDebugCss = new Lazy<string>(() => ReadResource("Templates.Debug.css"));

        public static Lazy<string> ImageViewCss = new Lazy<string>(() => ReadResource("Templates.ImageView.css"));
        
        public static Lazy<string> JQuery = new Lazy<string>(() =>
        {
            try
            {
                using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("BrowserUI.Scripts.jquery-3.3.1.min.js"))
                using (StreamReader reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
            catch (Exception)
            {
                //TODO Log
                Debug.WriteLine("Error read resource JQuery");
                return "alert ('Error #123 JQuery');";
            }
        });

        public static Lazy<string> ImageViewTs = new Lazy<string>(() => ReadResource("Scripts.ImageView.js"));

        public static string ReadResource(string resourcePath)
        {
            Contract.Assert(resourcePath != null);
            using (StreamReader reader = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream($"BrowserUI.{resourcePath}")))
            {
                return reader.ReadToEnd();
            }
        }
    }


    
}
