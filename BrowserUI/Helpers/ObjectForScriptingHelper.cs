﻿

using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Windows;

namespace BrowserUI.Helpers
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class ObjectForScriptingHelper
    {
        MainWindow mExternalWPF;
        public ObjectForScriptingHelper(MainWindow w)
        {
            this.mExternalWPF = w;
        }
        public void OnImageClick(string jsscript)
        {
            mExternalWPF.lblCursorPosition.Text = jsscript;
        }

    }
}
