﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using BrowserBL.Models;
using BrowserUI.Templates;
using MSHTML;
using DispHTMLDivElement = mshtml.DispHTMLDivElement;
using DispHTMLSpanElement = mshtml.DispHTMLSpanElement;

namespace BrowserUI.Helpers
{

    /// <summary>
    /// Helper для работы с Веб браузером
    /// </summary>
    public class WebBrowserHelper
    {
        public WebBrowserHelper(WebBrowser browser, double startWidth)
        {
            _browser = browser;
            StartWidth = startWidth;
            ImageWidth = (int)Math.Round(StartWidth * 0.15);
        }
        /// <summary>
        /// Минимальное расстояние между картинками для адаптационной вёрстки
        /// </summary>
        public int MarginPx = 20;
        /// <summary>
        /// Стартовая ширина картинки в браузере
        /// </summary>
        public int ImageWidth;
        private readonly WebBrowser _browser;
        /// <summary>
        /// Число колонок для рендера
        /// </summary>
        private int _columnsCount = 3;
        private readonly double StartWidth;

        public void LoadStylesToBrowser(IEnumerable<string> cssFileNames)
        {
            try
            {
                if (_browser.Document != null)
                {
                    IHTMLDocument2 currentDocument = (IHTMLDocument2)_browser.Document;
                    foreach (var style in cssFileNames)
                    {
                        int length = currentDocument.styleSheets.length;
                        IHTMLStyleSheet styleSheet = currentDocument.createStyleSheet(@"", length + 1);
                        styleSheet.cssText = style;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void LoadScriptsToBrowser(string[] scriptFileNames)
        {
            try
            {
                if (_browser.Document != null)
                {
                    IHTMLDocument2 currentDocument = (IHTMLDocument2)_browser.Document;
                    var doc = _browser.Document as HTMLDocument;
                    var body = doc.getElementsByTagName("body").Cast<HTMLBody>().First();
                    foreach (var scriptText in scriptFileNames)
                    {
                        var script = (IHTMLScriptElement)currentDocument.createElement("script");
                        script.text = scriptText;
                        body.appendChild((IHTMLDOMNode)script);

                    }


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void UpdateBrowser(IEnumerable<PreviewImage> added, IEnumerable<PreviewImage> removed)
        {
            var doc = (HTMLDocument)_browser.Document;
            var imageDiv = (DispHTMLDivElement)doc.getElementById("images");


            foreach (var addedImage in added)
            {
                //DEBUG
                var body = doc.getElementsByTagName("body").Cast<HTMLBody>().First();

                var imageId = addedImage.FullName.GetHashCode();
                var newImgDiv = (DispHTMLDivElement)doc.createElement("div");
                newImgDiv.setAttribute("className", "ImgContainer");
                newImgDiv.setAttribute("id", MainWindow.IMG_DIV_ID_PREFIX + imageId);


                var newImgSpan = (DispHTMLSpanElement)doc.createElement("span");
                newImgSpan.innerText = addedImage.FileName;

                var newImg = doc.createElement("img");//as mshtml.DispHTMLImg;
                //newImg.attachEvent("onclick", pdisp);
                //var evnt=doc.createEvent("onclick");
                //evnt.initEvent("onclick", false, true);

                //newImg.onclick+= new HTMLButtonElementEvents_onclickEventHandler//(buttonElement_HTMLButtonElementEvents_Event_onclick);
                //newImg.attachEvent("onclick", new EventListener);

                newImgDiv.appendChild(newImg as mshtml.IHTMLDOMNode);
                //newImgDiv.appendChild(doc.createElement("br") as mshtml.IHTMLDOMNode);
                newImgDiv.appendChild(newImgSpan as mshtml.IHTMLDOMNode);

                newImg.setAttribute("src", 
                    //addedImage.Image.UriSource.OriginalString
                    addedImage.FullName
                    );
                var asNode = newImgDiv as mshtml.IHTMLDOMNode;
                imageDiv.appendChild(asNode);

                _browser.InvokeScript("addImage", MainWindow.IMG_DIV_ID_PREFIX + imageId);
            }
            foreach (var removedImage in removed)
            {
                var idToDelete = MainWindow.IMG_DIV_ID_PREFIX + removedImage.FullName.GetHashCode();
                var divToDelete = doc.getElementById(idToDelete);
                imageDiv.removeChild(divToDelete as mshtml.IHTMLDOMNode);
            }
        }

        public void UpdateWebBrowserAdaptiveMarkup()
        {
            var rate = StartWidth / _browser.ActualWidth;

            //lblDebug.Text = this.Width.ToString(CultureInfo.InvariantCulture) + "/" + this.ActualWidth;
            if (rate > 3)
            {
                if (_columnsCount != 1)
                {
                    _columnsCount = 1;
                    _browser.InvokeScript("changeColumns", "100");
                }
                return;
            }

            if (rate > 2)
            {
                if (_columnsCount != 2)
                {
                    _columnsCount = 2;
                    _browser.InvokeScript("changeColumns", "50");
                }
                return;

            }

            if (rate <= 2 && _columnsCount != 3)
            {
                _columnsCount = 3;
                _browser.InvokeScript("changeColumns", "33");
            }
        }

        public void UpdateWebBrowserAdaptiveMarkupAlt()
        {
            
            
            if (_browser.ActualWidth > (MarginPx * 4 + ImageWidth * 3))
            {
                //Columns 3
                if (_columnsCount != 3)
                {
                    _columnsCount = 3;
                    _browser.InvokeScript("changeColumns", "33");
                }
                return;
            }
            if (_browser.ActualWidth > (MarginPx * 3 + ImageWidth * 2))
            {
                //Columns 3
                if (_columnsCount != 2)
                {
                    _columnsCount = 2;
                    _browser.InvokeScript("changeColumns", "50");
                }
                return;
            }

            if (_columnsCount == 1)
            {
                return;
            }

            _columnsCount = 1;
            _browser.InvokeScript("changeColumns", "100");
        }
    }


}
