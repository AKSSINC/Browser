﻿//alert('TS is loaded');



interface Window {
    External: External;
    selectedImage: HTMLImageElement;
}

interface External {
    OnImageClick: (param: string) => void;
}


function callServer(param: string) {
    //alert(param);
    window.external.OnImageClick(param);
}

function onImageSelected(element: HTMLDivElement, imageId: string) {
    if (window.selectedImage != null) {
        window.selectedImage.className = "";
    }

    window.selectedImage = element.children[0] as HTMLImageElement;
    window.selectedImage.className = "selected";
    

    //(<HTMLImageElement>window.selectedImage).classList.add("selected");
    var path = ""+ window.selectedImage.getAttribute('src');
    callServer(path);
}

function addImage(imageId: string) {
    var element = document.getElementById(imageId) as HTMLDivElement;
    
    if (element == null) {
        return;
    }
    var image = element.children[0] as HTMLImageElement;
    addListener(image, 'click', () => { onImageSelected(element, imageId); });
}

function addListener(element, eventName, handler) {
    if (element.addEventListener) {
        element.addEventListener(eventName, handler, false);
    }
    else if (element.attachEvent) {
        element.attachEvent('on' + eventName, handler);
    }
    else {
        element['on' + eventName] = handler;
    }
}

function removeListener(element, eventName, handler) {
    if (element.addEventListener) {
        element.removeEventListener(eventName, handler, false);
    }
    else if (element.detachEvent) {
        element.detachEvent('on' + eventName, handler);
    }
    else {
        element['on' + eventName] = null;
    }
}
//function writeDebug(msg: string) {
//    var debugDiv = document.getElementById('debug') as HTMLDivElement;
//    debugDiv.innerHTML = (new Date()).getDate()+' '+ msg;
//}

function changeColumns(colWidth: string) {

    //writeDebug('changeColumns: ' + colWidth);
    var sheetToBeRemoved =<any>document.getElementById('dcss')as any;
    var sheetParent = sheetToBeRemoved.parentNode;
    sheetParent.removeChild(sheetToBeRemoved);


    var sheet = document.createElement('style');
    sheet.id = 'dcss';
    sheet.innerHTML = '.ImgContainer { width: '+colWidth+'%; }';
    document.body.appendChild(sheet);


}
