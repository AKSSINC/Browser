//alert('TS is loaded');
function callServer(param) {
    //alert(param);
    window.external.OnImageClick(param);
}
function onImageSelected(element, imageId) {
    if (window.selectedImage != null) {
        window.selectedImage.className = "";
    }
    window.selectedImage = element.children[0];
    window.selectedImage.className = "selected";
    //(<HTMLImageElement>window.selectedImage).classList.add("selected");
    var path = "" + window.selectedImage.getAttribute('src');
    callServer(path);
}
function addImage(imageId) {
    var element = document.getElementById(imageId);
    if (element == null) {
        return;
    }
    var image = element.children[0];
    addListener(image, 'click', function () { onImageSelected(element, imageId); });
}
function addListener(element, eventName, handler) {
    if (element.addEventListener) {
        element.addEventListener(eventName, handler, false);
    }
    else if (element.attachEvent) {
        element.attachEvent('on' + eventName, handler);
    }
    else {
        element['on' + eventName] = handler;
    }
}
function removeListener(element, eventName, handler) {
    if (element.addEventListener) {
        element.removeEventListener(eventName, handler, false);
    }
    else if (element.detachEvent) {
        element.detachEvent('on' + eventName, handler);
    }
    else {
        element['on' + eventName] = null;
    }
}
//function writeDebug(msg: string) {
//    var debugDiv = document.getElementById('debug') as HTMLDivElement;
//    debugDiv.innerHTML = (new Date()).getDate()+' '+ msg;
//}
function changeColumns(colWidth) {
    //writeDebug('changeColumns: ' + colWidth);
    var sheetToBeRemoved = document.getElementById('dcss');
    var sheetParent = sheetToBeRemoved.parentNode;
    sheetParent.removeChild(sheetToBeRemoved);
    var sheet = document.createElement('style');
    sheet.id = 'dcss';
    sheet.innerHTML = '.ImgContainer { width: ' + colWidth + '%; }';
    document.body.appendChild(sheet);
}
//# sourceMappingURL=ImageView.js.map