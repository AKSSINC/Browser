﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.BrowserBL
{
    public class DirectoryModel
    {

        public DirectoryModel(DirectoryInfo dirInfo)
        {
            DirInfo = dirInfo;
        }

        public DirectoryInfo DirInfo;

        public string Name => DirInfo.Name;

        public List<DirectoryModel> Children
        {
            get
            {
                //Debug.WriteLine($"Open folder {DirInfo.Name}");
                var result = new List<DirectoryModel>();
                foreach (var child in DirInfo.GetDirectories())
                {
                    result.Add(new DirectoryModel(child));
                }

                return result;
            }
        }
    }
}
