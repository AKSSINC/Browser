﻿using System;
using System.IO;
using System.Windows.Media.Imaging;

namespace BrowserBL.Models
{
    public class PreviewImage
    {
        /// <summary>
        ///     Название файла
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        ///     Полный путь до файла
        /// </summary>
        public string FullName { get; set; }

        public static PreviewImage OpenImage(FileInfo fileInfo)
        {
            try
            {
                return new PreviewImage(fileInfo);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public PreviewImage(FileInfo fileInfo)
        {
            FullName = fileInfo.FullName;
            FileName = fileInfo.Name;
        }
    }
}